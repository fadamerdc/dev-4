<?xml version="1.0"?>
<project name="The Flow Repository Helper" default="init" basedir="" xmlns:sf="antlib:com.salesforce" >
	<!-- 
		Initiator:		Reinier van den Assum (Deloitte Consulting NL)
		Stage history:	See Changelog.txt
		
		Details:		ANT script to keep only the active flows in the /flow directory
						Only flows in /flow are deployed to Salesforce Instance
						Note, this ANT scripts adds the /flowsBackup directory to the repository
		
		Functionality:	1) Get all flowDefinitions from the target org -> /flowsDefTarget
						1) MOVE all /flows to /flowsBackup
						2) Loop over /flowDefinitions (Source)
							If		[active version in Source]
									If 		[flowDef in /flowDefTarget]
											If 		[active version in Target]
													- compare versionNumber
														Source > Target			COPY
														Source == Target		SKIP
														Source < Target			QUIT - Throw failure
											Else	[no active version in Target]
													- COPY
									Else	[flowDef not in /flowDefTarget]
											- COPY
							Else	[no active version in Source]
									- SKIP	
							
		Output:			The decision for each flow is mentioned in the output:
						+ indicates that the flow is included in the next deployment (copied to /flows)
						- indicates that the flow is not included in the next deployment	
	-->

	<property file="build.properties" />

	<taskdef resource="net/sf/antcontrib/antcontrib.properties">
		<classpath>
			<pathelement location="/build/lib/antcontrib/ant-contrib-1.0b2.jar"/>
		</classpath>
	</taskdef>

	<!-- 
		MAIN FUNCTION / CONSTRUCTOR
	-->
	<target name="init">
		<echo message="#### START FLOW REPOSITORY CHECK" />
		<echo message="This is ${script.name} v${script.version}" />
		
		<!-- Check whether required directories are available -->
		<if>
			<bool>	<available file="${dir.flows}" type="dir" /> 	</bool>
			<then>
				<if>
					<bool>	<available file="${dir.flowDefinitions}" type="dir" /> </bool>
					<then>
						<!-- Script can run -->
						<antcall target="retrieveFlowDefsFromTarget" />
						<antcall target="backupAllFlows" 	/>
						<antcall target="copyActiveFlows" />
						<antcall target="deleteFlowDefsFromTarget" />
						<echo message="#### END FLOW REPOSITORY CHECK" />
					</then>
					<else>
						<echo message="ANT was not able to find the required directories '${dir.flowDefinitions}'" />
						<fail message="FlowDefinitions directory not found" />
					</else>
				</if>
			</then>
			<else>
				<echo message="ANT was not able to find the required directories '${dir.flowDefinitions}'" />
				<echo message="Flow deployment preparation is skipped (${dir.flows})" />
			</else>
		</if>
	</target>

	<!--
		SUBFUNCTIONS OF CONSTRUCTOR
	-->
	<!-- Function to derive FlowDefinitions from Target, unless the setting is to skip this step 
		 If skipped, force the Target FlowDefinitions to exists as folder -->
	<target name="retrieveFlowDefsFromTarget">
		<if>
			<bool><isfalse value="${script.skipTargetConnection}" /></bool>
			<then>
				<!-- <antcall target="resetFlowDefTargetFolder" /> -->
				<antcall target="retrieveFlowDefinitionsFromTargetOrg" />
			</then>
			<else>
				<echo message="NOTE! The retrieval of Target FlowDefinitions is skipped due to the configuration in build.properties" />
				<if>
					<not> 	<available file="${dir.flowDefTarget.files}" type="dir" /> </not>
					<then>
						<fail message="You skipped flowDefinitions from Target but haven't set them!" />
					</then>
				</if>
			</else>
		</if>
	</target>
	
		<!-- Function to remove the folder if it remained (due to error) and create freshly new one -->
		<target name="resetFlowDefTargetFolder">
			<echo message="Creating new folder (${dir.flowDefTarget})" />
			
			<delete dir="${dir.flowDefTarget}" includeemptydirs="true" failonerror="false" />
			<mkdir dir="${dir.flowDefTarget}" />
		</target>
	
	<!-- Function to backup all flows -->
	<target name="backupAllFlows">
		<echo message="Backup flows from ${dir.flows} to ${dir.flows.backup}" />
		
			<!-- Make directory if not yet exists -->
		<mkdir dir="${dir.flows.backup}" />
		
			<!-- move all flows to (newly created) backup folder -->
		<move 	preservelastmodified	= "true"
				overwrite				= "true"
				todir					= "${dir.flows.backup}"
				failonerror				= "true" >
			<fileset dir="${dir.flows}">
				<include name="*.flow" />
			</fileset>
		</move>
	</target>
	
	<!-- Function to walk over each flowDefinition and add the referenced flow to the Flow Directory -->
	<target name="copyActiveFlows">
		<echo message="Copy flows back to ${dir.flows} dependent on Target org" />
		<antcall target="determineActiveFlowsAndCopy" />
	</target>
	
	<!-- Function to delete the earlier retrieved FlowDefinition of Target from repository -->
	<target name="deleteFlowDefsFromTarget">
		<echo message="Remove the flowDefinitions of the target org" />
		<delete dir="${dir.flowDefTarget}" includeemptydirs="true" failonerror="false" />
	</target>
	
	<!--
		HELPER FUNCTIONS 
	-->
	
	<!-- Function to retrieve FlowDefinitions from the Target Org -->
	<target name="retrieveFlowDefinitionsFromTargetOrg">		
		<echo message="Unzipping FlowDefinitions from Target: ${sf.username}" />

		<!--
		<exec executable="sh" osfamily="unix">
			<arg line="${script.sfdx}/sfdx force:mdapi:retrieve -a ${sf.apiversion} -r ${dir.flowDefTarget} -k ${basedir}/package.xml -u ${sf.username} -json" />
		</exec>
		<exec executable="sfdx" osfamily="windows">
			<arg line=" force:mdapi:retrieve -a ${sf.apiversion} -r ${dir.flowDefTarget} -k ${basedir}/package.xml -u ${sf.username} -json" />
		</exec>
		-->

		<unzip src="${dir.flowDefTarget}/unpackaged.zip" dest="${dir.flowDefTarget}"/>
		<delete file="${dir.flowDefTarget}/unpackaged.zip" failonerror="false" />
	</target>

	<!--
		Per flowDefinition in /flowDefinition (Source)
		If		[active version in Source]
				If 		[flowDef in /flowDefTarget]
						If 		[active version in Target]
								- compare versionNumber
									Source > Target			COPY
									Source == Target		SKIP
									Source < Target			QUIT - Throw failure
						Else	[no active version in Target]
								- COPY
				Else	[flowDef not in /flowDefTarget]
						- COPY
		Else	[no active version in Source]
				- SKIP
	-->
	<target name="determineActiveFlowsAndCopy">
		<script language="javascript">
		<![CDATA[        
		// Ensure we're in compatibility mode
		try {
			load("nashorn:mozilla_compat.js");
		} catch (e) {
		   throw new Error('Unable to load mozilla compatibility library');
		}
			
		// Load the file
		importClass(javax.xml.parsers.DocumentBuilder);
		importClass(javax.xml.parsers.DocumentBuilderFactory);
		importClass(org.xml.sax.InputSource);
		importClass(java.io.StringReader);
		importClass(java.util.Scanner);
		importClass(java.io.File);
		importClass(java.nio.file.Files);
		importClass(java.nio.file.Path);
		importClass(java.nio.file.Paths);
		importClass(java.nio.file.StandardCopyOption);
		
		// Create echo task
		var echo = project.createTask("echo");
		
		// Determine the version numbers for both the source and target
		var determineVersionNum = function(flowDefFile){
			var file = flowDefFile;
			var content = new Scanner(file).useDelimiter("\\Z").next();
			var dbFactory = DocumentBuilderFactory.newInstance();
			var dBuilder = dBuilder = dbFactory.newDocumentBuilder();
			var xmlDoc = dBuilder.parse( new InputSource(new StringReader(content)) );
			var activeVersionNumbers = xmlDoc.getElementsByTagName('activeVersionNumber');
			
			if(activeVersionNumbers.length > 0){
				return parseInt( activeVersionNumbers.item(0).getTextContent() );
			} else {
				return null;
			}
		};
		
		// Loop over all of the flow definitions
		var folder = new File( project.getProperty('dir.flowDefinitions') );
		var flowDefinitions = folder.listFiles();
		
		for (var i=0,j=flowDefinitions.length; i < j; i++) {
			if(flowDefinitions[i].isFile()){
				// Set up the parameters required for the flow repo handler to compare the source against the target
				var flowDefFileName = flowDefinitions[i].getName();
				var sourceFile = flowDefinitions[i];
				var targetFlowDefFileName = project.getProperty('dir.flowDefTarget.files') + '/' + flowDefFileName;
				var targetFlowDefFilePath = Paths.get(targetFlowDefFileName);
				var flowName = flowDefFileName.substring(0, flowDefFileName.indexOf('.flowDefinition'));
				var sourceVersionNum = determineVersionNum( sourceFile );
				var targetFlowDefFile = null;
			
				if(Files.exists(targetFlowDefFilePath)){
					targetFlowDefFile = new File(targetFlowDefFileName);
				}
			
				var targetVersionNum = (targetFlowDefFile != null) ? determineVersionNum( targetFlowDefFile ) : null;
				
				// Set up the flow file name using the extracted source version number
				var sourceFlowFileName = flowName + '-' + sourceVersionNum + '.flow';
				var copyToFlowsDir = false;
			
				if(sourceVersionNum != null){
					// Little trick: in case the xml property cannot be derived from the file, the tag is not substituted
					var isFlowPresentInTarget = (targetFlowDefFile != null);
					
					if(!isFlowPresentInTarget){
						// New flow being added
						echo.setMessage('+ New flow (S: ' + sourceVersionNum + ') [' + flowName + ']');
						echo.perform();
						copyToFlowsDir = true;
					} else {
						// The flowDefinition does EXIST, thus compare
						
						// Check whether the flow (in Target) has an active version
						if(targetVersionNum != null){
							// Compare the version Numbers	
							if(sourceVersionNum > targetVersionNum){
								echo.setMessage('+ Newer version (S: ' + sourceVersionNum + ' > T: ' + targetVersionNum + ') [' + flowName + ']');
								echo.perform();
								copyToFlowsDir = true;
				
							} else if(sourceVersionNum == targetVersionNum){
								// Same version, nothing to do
								echo.setMessage('- Same version (S: ' + sourceVersionNum + ' = T: ' + targetVersionNum + ') [' + flowName + ']');
								echo.perform();			
							
							} else {
								// Source has a newer flow activated
								throw new Error('! Older version (S: ' + sourceVersionNum + '< T: ' + targetVersionNum + ') [' + flowName + ']');
							}
				
						} else {
							// Flow is not active in Target, thus simply copy flow
							echo.setMessage('+ Flow not active in Target [' + flowName + ']');
							echo.perform();
							copyToFlowsDir = true;
						}
					}
				
				} else {
					// Source is not even active, so we delete it
					echo.setMessage('- Flow not active in Source [' + flowName + ']');
					echo.perform();
					sourceFile.delete();
				}
				
				if(copyToFlowsDir){
					var backupFolder = project.getProperty('dir.flows.backup');
					var outputFolder = project.getProperty('dir.flows');
					
					var sourceFlow = backupFolder + '/' + flowName + '-' + sourceVersionNum + '.flow';
					var targetFlow = outputFolder + '/' + flowName + '-' + sourceVersionNum + '.flow';
			
					Files.copy(Paths.get(sourceFlow), Paths.get(targetFlow), StandardCopyOption.REPLACE_EXISTING);
				}
			}
		}]]></script>
	</target>
</project>